#include "FKObjectManager.h"

namespace FrameKit
{

	FKObjectManager::FKObjectManager(OMinfo* info)
	{
		HRESULT hr = S_OK;    //���������

								//���� ���������
		D3D_DRIVER_TYPE l_driverTypes[] =
		{
			D3D_DRIVER_TYPE_HARDWARE,
			D3D_DRIVER_TYPE_WARP,
			D3D_DRIVER_TYPE_REFERENCE,
		};

		UINT l_numDriverTypes = ARRAYSIZE(l_driverTypes);  //�� ����������



														   //���������� ������ API
		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0,
		};
		UINT l_numFeatureLevels = ARRAYSIZE(featureLevels);  //�� ����������

		DXGI_SWAP_CHAIN_DESC l_sd;			//��������� ���� ��������
		ZeroMemory(&l_sd, sizeof(l_sd));

		//���������
		l_sd.BufferCount = info->bufferCount;
		l_sd.BufferDesc.Width = info->width;
		l_sd.BufferDesc.Height = info->height;
		l_sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		l_sd.BufferDesc.RefreshRate.Numerator = 60;
		l_sd.BufferDesc.RefreshRate.Denominator = 1;
		l_sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		l_sd.OutputWindow = info->window;
		l_sd.SampleDesc.Count = info->multisamples;
		l_sd.SampleDesc.Quality = info->quality;
		l_sd.Windowed = info->windowed;
		l_sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
		//-------------------------------------------------

		UINT createDeviceFlags = 0;
#ifdef _DEBUG
		createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

		for (UINT driverTypeIndex = 0; driverTypeIndex < l_numDriverTypes; driverTypeIndex++)
		{
			hr = S_OK;
			if (!info->refDevice)
				p_DriverType = l_driverTypes[driverTypeIndex];
			else
				p_DriverType = D3D_DRIVER_TYPE_REFERENCE;
			hr = D3D11CreateDeviceAndSwapChain(NULL, p_DriverType, NULL, createDeviceFlags, featureLevels, l_numFeatureLevels,
				D3D11_SDK_VERSION, &l_sd, &p_SwapChain, &p_Device, &p_FeatureLevel, &p_DrawContext);
			if (SUCCEEDED(hr))
				break;
		}
		if (FAILED(hr))
			ERROR("FKObjectManager: �� ������� ������� ����������.");


		SetViewPort(info->width, info->height, 0, 0);
		p_DrawContext->IASetPrimitiveTopology(p_PrimitiveTopology);


#ifdef _DEBUG
		p_Device->QueryInterface(IID_PPV_ARGS(&p_Debug));
#endif

	}

	FKObjectManager::~FKObjectManager()
	{
		for (int i = 0; i < p_ObjectList.size(); i++)
		{
		FKObject* obj = p_ObjectList[i];
		obj->Release();
		}

		SAFE_RELEASE(p_Debug);
		SAFE_RELEASE(p_SwapChain);
		SAFE_RELEASE(p_DrawContext);
		SAFE_RELEASE(p_Device);
	}

	ID3D11Device * FKObjectManager::GetDevice()
	{
		return p_Device;
	}

	IDXGISwapChain * FKObjectManager::GetSwapChain()
	{
		return p_SwapChain;
	}

	ID3D11DeviceContext * FKObjectManager::GetDrawContext()
	{
		return p_DrawContext;
	}

	void FKObjectManager::SetViewPort(float w, float h, int x, int y)
	{
		D3D11_VIEWPORT vp;
		vp.Width = w;
		vp.Height = h;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = x;
		vp.TopLeftY = y;
		p_DrawContext->RSSetViewports(1, &vp);
	}

	void FKObjectManager::CreateRT(RTinfo* info, FKRenderTarget *& rt)
	{
		rt = new FKRenderTarget(info,this);
		p_ObjectList.push_back((FKObject*)rt);
	}

	void FKObjectManager::CreateS(Sinfo* info, FKShader *& s)
	{
		s = new FKShader(info, this);
		p_ObjectList.push_back((FKObject*)s);
	}

	void FKObjectManager::CreateCB(CBinfo * info, FKConstantBuffer *& cb)
	{
		cb = new FKConstantBuffer(info, this);
		p_ObjectList.push_back((FKObject*)cb);
	}

	void FKObjectManager::CreateSM(FKStaticMesh*& sm)
	{
		sm = new FKStaticMesh(this);
		p_ObjectList.push_back((FKObject*)sm);
	}

	void FKObjectManager::CreateT(Tinfo * info, FKTexture *& t)
	{
		t = new FKTexture(info, this);
		p_ObjectList.push_back((FKObject*)t);
	}

	void FKObjectManager::CreateSB(SBinfo * info, FKStructuredBuffer *& sb)
	{
		sb = new FKStructuredBuffer(info, this);
		p_ObjectList.push_back((FKObject*)sb);
	}


}

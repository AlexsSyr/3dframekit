#pragma once
#include"FKTransformation.h"
namespace FrameKit
{

	class FKCamera
	{
	public:
		FKCamera();

		XMFLOAT3 GetPosition()const;
		void SetPosition(float x, float y, float z);
		void SetViewMatrix(XMMATRIX* view);
		void GetViewMatrix(XMMATRIX& m);
		void GetForvardVector(XMFLOAT3& vec);
		

		void Strafe(float d);
		void Walk(float d);
		// Rotate the camera. 
		void Pitch(float angle);
		void LookAt(XMFLOAT3 pos, XMFLOAT3 target, XMFLOAT3 worldUp);
		void RotateY(float angle);
		// After modifying camera position/orientation, call // to rebuild the view matrix once per frame. 
		void UpdateViewMatrix();


	private:
		// Camera coordinate system with coordinates relative to world space. 
		XMFLOAT3 mPosition = XMFLOAT3(0, 0, 0);
		// view space origin 
		XMFLOAT3 mRight = XMFLOAT3(1, 0, 0);
		// view space x-axis
		XMFLOAT3 mUp = XMFLOAT3(0, 1, 0);
		// view space y-axis 
		XMFLOAT3 mLook = XMFLOAT3(0, 0, 1);     // view space z-axis 

		XMMATRIX mView;								// Cache frustum properties. 

	
	};

}

#include "FKStructuredBuffer.h"


namespace FrameKit
{


	FKStructuredBuffer::FKStructuredBuffer(SBinfo* info, FKObjectManager* om)
	{
		p_OM = om;
		p_ForWrite = info->forWrite;

		D3D11_BUFFER_DESC l_desc;
		l_desc.Usage = D3D11_USAGE_DEFAULT;
		l_desc.ByteWidth = info->size;
		l_desc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
		l_desc.CPUAccessFlags = 0;
		l_desc.StructureByteStride = info->structSize;
		l_desc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;


		p_OM->GetDevice()->CreateBuffer(&l_desc, NULL, &p_Buffer);



		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = DXGI_FORMAT_UNKNOWN;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFEREX;
		srvDesc.BufferEx.FirstElement = 0;
		srvDesc.BufferEx.Flags = 0;
		srvDesc.BufferEx.NumElements = info->size / info->structSize;

		p_OM->GetDevice()->CreateShaderResourceView(p_Buffer, &srvDesc, &p_CSRes);


		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		uavDesc.Format = DXGI_FORMAT_UNKNOWN;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;
		uavDesc.Buffer.FirstElement = 0;
		uavDesc.Buffer.Flags = 0;
		uavDesc.Buffer.NumElements = info->size / info->structSize;
		p_OM->GetDevice()->CreateUnorderedAccessView(p_Buffer, &uavDesc, &p_CSRWRes);




	}

	void FKStructuredBuffer::SetData(void * l_data)
	{
		p_OM->GetDrawContext()->UpdateSubresource(p_Buffer, 0, NULL, l_data, 0, 0);
	}

	void FKStructuredBuffer::SetBuffer(UINT l_num)
	{
		if (!p_ForWrite)
			p_OM->GetDrawContext()->CSSetShaderResources(l_num, 1, &p_CSRes);
		else
			p_OM->GetDrawContext()->CSSetUnorderedAccessViews(l_num, 1, &p_CSRWRes, NULL);
	}

	void FKStructuredBuffer::Release()
	{
		SAFE_RELEASE(p_Buffer);
		SAFE_RELEASE(p_CSRWRes);
		SAFE_RELEASE(p_CSRes);
	}

}
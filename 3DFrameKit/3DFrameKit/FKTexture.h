#pragma once
#include"General.h"
#include"FKObject.h"
#include"FKObjectManager.h"

namespace FrameKit
{

	class FKObjectManager;

	class FKTexture : public FKObject
	{


	public:
		FKTexture(Tinfo* info, FKObjectManager* om);
	

		HRESULT CreateSampler();
		HRESULT CreateTexture();
		void    SetTexture(UINT numSlot);
		void    SetSampler(UINT numSlot);
		bool    InitForRendering();
		void    SetForRendering();
		void    Release();
		void    CreateForCSOutput();
		void    SetCSOutput(UINT numSlot);

	private:
		ID3D11ShaderResourceView*  p_TextureRes          = NULL;
		ID3D11SamplerState*        p_Sampler             = NULL;
		ID3D11UnorderedAccessView* p_CSTextureRes        = NULL;
		ID3D11RenderTargetView*    p_TextureRenderTarget = NULL;
		ID3D11DepthStencilView*    p_DepthStencilView    = NULL;
		FKObjectManager*           p_OM                  = NULL;
		Tinfo                      p_Tinfo;
		float                      p_ClearColor[4] = { 0.6f, 0.6f, 0.6f, 1.0f };


	};

}
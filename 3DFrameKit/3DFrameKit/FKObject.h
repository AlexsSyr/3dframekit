#pragma once

namespace FrameKit
{

	class FKObject
	{
	public:
		virtual void Release() = 0;
	};

}


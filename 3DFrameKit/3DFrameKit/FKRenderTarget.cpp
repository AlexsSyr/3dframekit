#include "FKRenderTarget.h"

namespace FrameKit
{

	FKRenderTarget::FKRenderTarget(RTinfo* info, FKObjectManager* om)
	{
		HRESULT           hr                 = S_OK;
		ID3D11Texture2D** textureArray       = NULL;

		p_OM = om;

		p_BufferCount = info->bufferCount;

		textureArray              = new ID3D11Texture2D*[p_BufferCount];
		p_TargetViewArray         = new ID3D11RenderTargetView*[p_BufferCount];
		

		if (p_BufferCount != 1)
		{

			p_ShaderResourceViewArray = new ID3D11ShaderResourceView*[p_BufferCount];
			p_CSRenderTargetRes = new ID3D11UnorderedAccessView*[p_BufferCount];

			D3D11_TEXTURE2D_DESC* textureDesc = NULL;
			textureDesc = new D3D11_TEXTURE2D_DESC();

			textureDesc->MipLevels = 1;
			textureDesc->ArraySize = 1;
			textureDesc->Format = info->format;
			textureDesc->SampleDesc.Count = info->multisamples;
			textureDesc->SampleDesc.Quality = info->quality;
			textureDesc->Usage = D3D11_USAGE_DEFAULT;
			textureDesc->BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS;
			textureDesc->CPUAccessFlags = 0;
			textureDesc->MiscFlags = 0;
			textureDesc->Width = info->width;
			textureDesc->Height = info->height;




			for (int i = 0; i < p_BufferCount; i++)
			{
				hr = p_OM->GetDevice()->CreateTexture2D(textureDesc, NULL, &textureArray[i]);
				if (FAILED(hr))
				{
					ERROR("FKRenderTarget: ������ �������� ��������.");
				}
			}

			D3D11_RENDER_TARGET_VIEW_DESC renderTargetViewDesc;

			renderTargetViewDesc.Format = textureDesc->Format;
			renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			renderTargetViewDesc.Texture2D.MipSlice = 0;

			for (int i = 0; i < p_BufferCount; i++)
			{
				hr = p_OM->GetDevice()->CreateRenderTargetView(textureArray[i], &renderTargetViewDesc, &p_TargetViewArray[i]);
				if (FAILED(hr))
				{
					ERROR("FKRenderTarget: ������ �������� ���� ��� ���������.");
				}
			}

			D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;

			shaderResourceViewDesc.Format = textureDesc->Format;
			shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
			shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
			shaderResourceViewDesc.Texture2D.MipLevels = 1;

			D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;

			uavDesc.Format = textureDesc->Format;
			uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
			uavDesc.Texture2D.MipSlice = 0;

			for (int i = 0; i < p_BufferCount; i++)
			{
				hr = S_OK;
					hr = p_OM->GetDevice()->CreateShaderResourceView(textureArray[i], &shaderResourceViewDesc, &p_ShaderResourceViewArray[i]);
				if (FAILED(hr))
				{
					ERROR("FKRenderTarget: ������ �������� ������ ��� ���� ���������.");
				}

						hr = p_OM->GetDevice()->CreateUnorderedAccessView(textureArray[i], &uavDesc, &p_CSRenderTargetRes[i]);
				if (FAILED(hr))
				{
					ERROR("FKRenderTarget: ������ �������� ������������ ������ ��� ���� ���������.");
				}
			}

			for (int i = 0; i < p_BufferCount; i++)
			{
				SAFE_RELEASE(textureArray[i])
			}

		}
		else
		{

			ID3D11Texture2D* backBuffer = NULL;
			hr = p_OM->GetSwapChain()->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);
			if (FAILED(hr))	ERROR(" �� ������: # 0002");

			hr = p_OM->GetDevice()->CreateRenderTargetView(backBuffer, NULL, &p_TargetViewArray[0]);
			if (FAILED(hr))	ERROR(" �� ������: # 0003");
			SAFE_RELEASE(backBuffer);
		}

		if (info->zBuffer)
			CreateZbuffer();

		


	}


	void FKRenderTarget::CreateZbuffer()
	{
		HRESULT hr = S_OK;

		ID3D11Texture2D* backBuffer = NULL;
	    p_OM->GetSwapChain()->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);

		D3D11_TEXTURE2D_DESC descDepth;            // ��������� � �����������
		ZeroMemory(&descDepth, sizeof(descDepth));
		backBuffer->GetDesc(&descDepth);
		descDepth.MipLevels = 1;                   // ������� ������������
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT; // ������ (������ �������)
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;         // ��� - ����� ������
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;

		ID3D11Texture2D*  depthStencilBuffer = NULL;

		hr = p_OM->GetDevice()->CreateTexture2D(&descDepth, NULL, &depthStencilBuffer);
		if(FAILED(hr))
			ERROR("FKRenderTarget: ������ �������� �������� ��� Z-������.");

		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;          
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = descDepth.Format;              
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;

		hr = p_OM->GetDevice()->CreateDepthStencilView(depthStencilBuffer, &descDSV, &p_DepthStencilView);
		if (FAILED(hr))
			ERROR("FKRenderTarget: ������ �������� Z-������.");

		SAFE_RELEASE(backBuffer);
		SAFE_RELEASE(depthStencilBuffer);

	}

	void FKRenderTarget::Clear()
	{
		for (int i = 0; i < p_BufferCount; i++)
		{
			if(p_TargetViewArray[i])
			p_OM->GetDrawContext()->ClearRenderTargetView(p_TargetViewArray[i], p_ClearColor);
		}
		if (p_DepthStencilView)
			p_OM->GetDrawContext()->ClearDepthStencilView(p_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	void FKRenderTarget::Show(bool vSync)
	{
		p_OM->GetSwapChain()->Present(vSync, 0);
	}

	void FKRenderTarget::Release()
	{
		for (int i = 0; i < p_BufferCount; i++)
		{
			if (p_TargetViewArray)
				SAFE_RELEASE(p_TargetViewArray[i]);
			if (p_ShaderResourceViewArray)
				SAFE_RELEASE(p_ShaderResourceViewArray[i]);
			if (p_CSRenderTargetRes)
				SAFE_RELEASE(p_CSRenderTargetRes[i]);
		}
		SAFE_RELEASE(p_DepthStencilView);
		this->~FKRenderTarget();

	}

	void FKRenderTarget::SetRenderTarget()
	{
		if(p_DepthStencilView)
		if (p_BufferCount == 1)
			p_OM->GetDrawContext()->OMSetRenderTargets(p_BufferCount, &p_TargetViewArray[0], p_DepthStencilView);
		else
			p_OM->GetDrawContext()->OMSetRenderTargets(p_BufferCount, p_TargetViewArray, p_DepthStencilView);
		else
			p_OM->GetDrawContext()->OMSetRenderTargets(0, NULL, p_DepthStencilView);
	}

	void FKRenderTarget::SetClearColor(float r, float g, float b)
	{
		p_ClearColor[0] = r;
		p_ClearColor[1] = g;
		p_ClearColor[2] = b;
	}

	void FKRenderTarget::SetMRTres(UINT numSlot, UINT numBuffer, bool ps, bool cs)
	{
		if (ps)
			p_OM->GetDrawContext()->PSSetShaderResources(numSlot, 1, &p_ShaderResourceViewArray[numBuffer]);
		if (cs)
			p_OM->GetDrawContext()->CSSetShaderResources(numSlot, 1, &p_ShaderResourceViewArray[numBuffer]);
	}

	void FKRenderTarget::SetMRTCSOutput(UINT numSlot, UINT numBuffer)
	{
		p_OM->GetDrawContext()->CSSetUnorderedAccessViews(numSlot, 1, &p_CSRenderTargetRes[numBuffer], NULL);
	}

}
#include "FKConstantBuffer.h"

namespace FrameKit
{

	FKConstantBuffer::~FKConstantBuffer()
	{
	}

	FKConstantBuffer::FKConstantBuffer(CBinfo * info, FKObjectManager * om)
	{
		p_PS = info->ps;
		p_VS = info->vs;

		p_OM = om;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = info->size;
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		p_OM->GetDevice()->CreateBuffer(&bd, NULL, &p_ConstBuffer);
	}

	void FKConstantBuffer::SetData(void * data)
	{
		p_OM->GetDrawContext()->UpdateSubresource(p_ConstBuffer, 0, NULL, data, 0, 0);
	}

	void FKConstantBuffer::SetBuffer(UINT num)
	{
		if (p_VS)
			p_OM->GetDrawContext()->VSSetConstantBuffers(num, 1, &p_ConstBuffer);

		if (p_PS)
			p_OM->GetDrawContext()->PSSetConstantBuffers(num, 1, &p_ConstBuffer);
	}

	void FKConstantBuffer::Release()
	{
		SAFE_RELEASE(p_ConstBuffer);
	}

}
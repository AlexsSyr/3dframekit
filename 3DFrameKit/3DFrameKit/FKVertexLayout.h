#pragma once
#include"General.h"

namespace FrameKit
{

	class FKVertexLayout
	{
	public:
		FKVertexLayout();
		HRESULT AddVertexParam(char* name);

	protected:
		ID3D11InputLayout*        p_VertexLayoutList = NULL;		    //������ ������
		D3D11_INPUT_ELEMENT_DESC* p_Layout           = NULL;
		UINT                      p_VertexParamCount = 0;
		UINT                      p_PosCount         = 0;
		UINT                      p_ColorCount       = 0;
		UINT                      p_NormalCount      = 0;
		UINT                      p_TexCount         = 0;
		UINT                      p_TangCount        = 0;
	};
}


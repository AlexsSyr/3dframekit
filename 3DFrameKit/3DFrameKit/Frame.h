#pragma once
#include<Windows.h>
#define WINDOW_CLASS_NAME "GX API"
#define WINDOW_TITLE "GX API"
#define WINDOW_H 480
#define WINDOW_W 640
//////////////////////////////////////////////////////////////
#define DESCR_INIT HINSTANCE g_hInst = NULL; HWND hwnd = NULL;
#define PROT_INIT HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow); LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
#define MAIN_INIT int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPWSTR lpCmdLine, int nCmdShow){
#define WINDOW_PARAM_INT UNREFERENCED_PARAMETER(hPrevInstance); UNREFERENCED_PARAMETER(lpCmdLine); if (FAILED(InitWindow(hInstance, nCmdShow))) return 0;
#define WINDOW_LOOP MSG msg = { 0 }; while (WM_QUIT != msg.message){ if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)){ TranslateMessage(&msg); DispatchMessage(&msg); 	}else

#define CREATE_WINDOW HRESULT InitWindow(HINSTANCE hInstance, int nCmdShow){ \
WNDCLASSEX wcex;\
wcex.cbSize = sizeof(WNDCLASSEX);\
wcex.style = CS_HREDRAW | CS_VREDRAW;\
wcex.lpfnWndProc = WndProc;\
wcex.cbClsExtra = 0;\
wcex.cbWndExtra = 0;\
wcex.hInstance = hInstance;\
wcex.hIcon = NULL;\
wcex.hCursor = LoadCursor(NULL, IDC_ARROW);\
wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);\
wcex.lpszMenuName = NULL;\
wcex.lpszClassName = WINDOW_CLASS_NAME;\
wcex.hIconSm = LoadIcon(wcex.hInstance, NULL);\
if (!RegisterClassEx(&wcex))\
return E_FAIL;\
g_hInst = hInstance;\
RECT rc = { 0, 0, WINDOW_W, WINDOW_H };\
AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);\
hwnd = CreateWindow(WINDOW_CLASS_NAME, WINDOW_TITLE, WS_OVERLAPPEDWINDOW,\
	CW_USEDEFAULT, CW_USEDEFAULT, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, hInstance, NULL);\
if (!hwnd)\
return E_FAIL;\
ShowWindow(hwnd, nCmdShow);\
return S_OK;\
}

#define LOOP_CODE LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam){ \
PAINTSTRUCT ps;\
HDC hdc;\
switch (message)
////////////////////////////////////////
#define USE_GXWINDOW DESCR_INIT 
#define INIT_ACTION_CALL Init();
#define INIT_ACTION void Init()
#define WINDOW PROT_INIT MAIN_INIT  WINDOW_PARAM_INT INIT_ACTION_CALL WINDOW_LOOP
#define WINDOW_END }return (int)msg.wParam;} CREATE_WINDOW
#define LOOP LOOP_CODE
#define STEP default:return DefWindowProc(hWnd, message, wParam, lParam);}return 0;
////////////////////////////////////////
#define ACT_DESTROY case WM_DESTROY:PostQuitMessage(0);break;
/////////////////////////////////////////////////////////////
#include "FKVertexLayout.h"

namespace FrameKit
{

	FKVertexLayout::FKVertexLayout()
	{
	}

	HRESULT FKVertexLayout::AddVertexParam(char * name)
	{
		HRESULT hr = S_OK;

		UINT sizeByte = (p_PosCount + p_NormalCount) * 12 + p_ColorCount * 16 + p_TexCount * 8;
		D3D11_INPUT_ELEMENT_DESC* buffer = p_Layout;  //�������� ������ �� ������ ������� 
		p_Layout = NULL;                              //���������� ��������� �� �������� ������
		p_VertexParamCount++;

		p_Layout = new D3D11_INPUT_ELEMENT_DESC[p_VertexParamCount];

		for (int i = 0; i < p_VertexParamCount - 1; i++) {
			*(p_Layout + i) = *(buffer + i);
		}


		delete[] buffer;

		p_Layout[p_VertexParamCount - 1].SemanticName = name;
		p_Layout[p_VertexParamCount - 1].SemanticIndex = 0;
		if (!strcmp(name, "POSITION"))
		{
			p_Layout[p_VertexParamCount - 1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			p_Layout[p_VertexParamCount - 1].InputSlot = p_PosCount;
			p_PosCount++;
		}

		if (!strcmp(name, "COLOR"))
		{
			p_Layout[p_VertexParamCount - 1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
			p_Layout[p_VertexParamCount - 1].InputSlot = p_ColorCount;
			p_ColorCount++;
		}


		if (!strcmp(name, "NORMAL"))
		{
			p_Layout[p_VertexParamCount - 1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			p_Layout[p_VertexParamCount - 1].InputSlot = p_NormalCount;
			p_NormalCount++;
		}

		if (!strcmp(name, "TEXCOORD"))
		{
			p_Layout[p_VertexParamCount - 1].Format = DXGI_FORMAT_R32G32_FLOAT;
			p_Layout[p_VertexParamCount - 1].InputSlot = p_TexCount;
			p_TexCount++;
		}

		if (!strcmp(name, "TANGENT"))
		{
			p_Layout[p_VertexParamCount - 1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
			p_Layout[p_VertexParamCount - 1].InputSlot = p_TangCount;
			p_TangCount++;
		}


		p_Layout[p_VertexParamCount - 1].AlignedByteOffset = sizeByte;
		p_Layout[p_VertexParamCount - 1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
		p_Layout[p_VertexParamCount - 1].InstanceDataStepRate = 0;

		return hr;
	}

}
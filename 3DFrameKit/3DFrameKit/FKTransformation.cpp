#include "FKTransformation.h"


namespace FrameKit
{


	void FKTransformation::GetTransformMatrix(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale, XMMATRIX& matrix)
	{
		XMMATRIX res = XMMatrixIdentity();
		XMMATRIX Rot = XMMatrixRotationX(rot.x) * XMMatrixRotationY(rot.y) * XMMatrixRotationZ(rot.z);
		XMMATRIX Scale = XMMatrixScaling(scale.x, scale.y, scale.z);
		res = Scale * Rot * XMMatrixTranslation(pos.x, pos.y, pos.z);
		matrix = XMMatrixTranspose(res);
	}

	void FKTransformation::GetViewMatrix(XMFLOAT3 eye, XMFLOAT3 view, XMFLOAT3 up, XMMATRIX& matrix)
	{
		XMVECTOR Eye = XMVectorSet(eye.x, eye.y, eye.z, 0.0f);  // ������ �������
		XMVECTOR At = XMVectorSet(view.x, view.y, view.z, 0.0f);    // ���� �������
		XMVECTOR Up = XMVectorSet(up.x, up.y, up.z, 0.0f);    // ����������� �����
		matrix = XMMatrixTranspose(XMMatrixLookAtLH(Eye, At, Up));
	}

	void FKTransformation::GetProjectionMatrix(float angle, float aspect, float nearPlace, float farPlace, XMMATRIX & matrix)
	{
		matrix = XMMatrixTranspose(XMMatrixPerspectiveFovLH(angle, aspect, nearPlace, farPlace));
	}

	void FKTransformation::XMvectorTOfloat3(XMVECTOR vec, XMFLOAT3 & flo)
	{
		flo.x = XMVectorGetX(vec);
		flo.y = XMVectorGetY(vec);
		flo.z = XMVectorGetZ(vec);
	}

	void FKTransformation::UnProject(XMFLOAT3 p, XMFLOAT3 & pos, int x, int y, int w, int h, XMMATRIX* v, XMMATRIX* proj)
	{
	 XMVECTOR vec = 	XMVector3Unproject(XMVectorSet(p.x,p.y,p.z, 1), 0, 0, w, h, 0.0f, 1.0f, XMMatrixTranspose(*proj), XMMatrixTranspose(*v), XMMatrixIdentity());
	 pos.x = XMVectorGetX(vec);
	 pos.y = XMVectorGetY(vec);
	 pos.z = XMVectorGetZ(vec);
	}

	float FKTransformation::LineTracing(XMFLOAT3 point, XMFLOAT3 vec, XMFLOAT3 a, XMFLOAT3 b, XMFLOAT3 c, XMFLOAT3& res)
	{
		XMFLOAT3 v1;
		XMFLOAT3 v2;
		XMFLOAT3 v3;
		float d;
		float t;

		v1.x = a.x - b.x;
		v1.y = a.y - b.y;
		v1.z = a.z - b.z;

		v2.x = c.x - b.x;
		v2.y = c.y - b.y;
		v2.z = c.z - b.z;

		XMVECTOR v4 = XMVector3Cross(XMVectorSet(v1.x, v1.y, v1.z, 0), XMVectorSet(v2.x, v2.y, v2.z, 0));
		v3.x = XMVectorGetX(v4);
		v3.y = XMVectorGetY(v4);
		v3.z = XMVectorGetZ(v4);

		d = a.x*v3.x + a.y*v3.y + a.z*v3.z;
		d = -d;

		if (v3.x*vec.x + v3.y*vec.y + v3.z*vec.z == 0)
			return 0.0f;
		else
		{
			t = (-d - v3.x*point.x - v3.y*point.y - v3.z*point.z) / (v3.x*vec.x + v3.y*vec.y + v3.z*vec.z);
			res.x = point.x + vec.x*t;
			res.y = point.y + vec.y*t;
			res.z = point.z + vec.z*t;
			return t;
		}
	}

}
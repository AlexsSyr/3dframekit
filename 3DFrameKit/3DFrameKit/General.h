#pragma once
#pragma comment(lib,"d3d11.lib")
#pragma comment(lib,"d3dx11.lib")
#include<Windows.h>
#include<WinUser.h>
#include<D3D11.h>
#include<D3DX11.h>
#include"Frame.h"
using namespace std;

//������_���������/�����_���

#define ERROR(arg) {MessageBoxA(NULL, arg, "3DFrameKit Error !!!", MB_OK); PostQuitMessage(0); return;}  //����� ��������� �� ����������
#define SAFE_RELEASE(arg) if(arg){ arg->Release(); arg = NULL;}     //���������� ��������

struct RTinfo
{
	int         width;
	int         height;
	DXGI_FORMAT format;        //������
	long        multisamples;  //�����������
	int         quality;       //��������
	int         bufferCount;   //���������� �������
	bool        zBuffer;
};

struct OMinfo
{
	int         width;
	int         height;
	DXGI_FORMAT format;        //������
	long        multisamples;  //�����������
	int         quality;       //��������
	int         bufferCount;   //���������� �������
	bool        zBuffer;
	HWND        window;
	bool        windowed;
	bool        refDevice;
};

struct Sinfo
{
	LPWSTR fileName;
	LPCSTR vsInput;
	LPCSTR psInput;
	LPCSTR csInput;
};

struct CBinfo
{
	UINT size;
	bool vs;
	bool ps;
};

struct SBinfo
{
	UINT size;
	UINT structSize;
	bool forWrite;
};

struct Tinfo
{
	D3D11_FILTER filter;
	LPWSTR fileName;
	bool ps;
	bool cs;
	UINT textureWidth;
	UINT textureHeight;
	bool zBuffer;

};

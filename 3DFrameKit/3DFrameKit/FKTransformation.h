#pragma once
#include<D3D11.h>
#include<xnamath.h>

namespace FrameKit
{

	class FKTransformation
	{
	public:

		static void GetTransformMatrix(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale, XMMATRIX& matrix);
		static void GetViewMatrix(XMFLOAT3 eye, XMFLOAT3 view, XMFLOAT3 up, XMMATRIX& matrix);
		static void GetProjectionMatrix(float angle, float aspect, float nearPlace, float farPlace, XMMATRIX & matrix);
		static void XMvectorTOfloat3(XMVECTOR vec, XMFLOAT3& flo);
		static void UnProject(XMFLOAT3 p, XMFLOAT3 & pos, int x, int y, int w, int h, XMMATRIX* v, XMMATRIX* proj);
		static float LineTracing(XMFLOAT3 point, XMFLOAT3 vec, XMFLOAT3 a, XMFLOAT3 b, XMFLOAT3 c, XMFLOAT3& res);
	};

}
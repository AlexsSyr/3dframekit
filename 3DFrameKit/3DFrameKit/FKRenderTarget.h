#pragma once
#include"General.h"
#include"FKObject.h"
#include"FKObjectManager.h"
namespace FrameKit
{
	class FKObjectManager;
	
    class FKRenderTarget : public FKObject
	{      
		


		UINT                        p_BufferCount;
		ID3D11RenderTargetView**    p_TargetViewArray         = NULL;
		ID3D11ShaderResourceView**  p_ShaderResourceViewArray = NULL;
		ID3D11UnorderedAccessView** p_CSRenderTargetRes       = NULL;
		ID3D11DepthStencilView*     p_DepthStencilView        = NULL;
		float                       p_ClearColor[4]           = { 0.6f, 0.6f, 0.6f, 1.0f };
		FKObjectManager*            p_OM                      = NULL;
		ID3D11Texture2D*            p_TempTex                 = NULL;

		void CreateZbuffer();

	public:
		FKRenderTarget(RTinfo* info, FKObjectManager* om);
		void Clear();
		void Show(bool vSync);
		void Release();
		void SetRenderTarget();
		void SetClearColor(float r, float g, float b);
		void SetMRTres(UINT numSlot, UINT numBuffer, bool ps, bool cs);
		void SetMRTCSOutput(UINT numSlot, UINT numBuffer);
	};
}


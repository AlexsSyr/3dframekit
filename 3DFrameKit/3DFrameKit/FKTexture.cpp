﻿#include"FKTexture.h"

namespace FrameKit
{

	FKTexture::FKTexture(Tinfo * info, FKObjectManager * om)
	{
		p_OM = om;
		p_Tinfo = *info;
	}

	HRESULT FKTexture::CreateSampler()
	{
		HRESULT hr;
		D3D11_SAMPLER_DESC sampDesc;
		ZeroMemory(&sampDesc, sizeof(sampDesc));
		sampDesc.Filter = p_Tinfo.filter;
		sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
		sampDesc.MinLOD = 0;
		sampDesc.MaxLOD = D3D11_FLOAT32_MAX;

		hr = p_OM->GetDevice()->CreateSamplerState(&sampDesc, &p_Sampler);
		if (FAILED(hr)) return hr;

		return hr;
	}

	HRESULT FKTexture::CreateTexture()
	{
		HRESULT hr = S_OK;

		D3DX11_IMAGE_LOAD_INFO info;
		info.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

		hr = D3DX11CreateShaderResourceViewFromFileW(p_OM->GetDevice(), p_Tinfo.fileName, &info, NULL, &p_TextureRes, NULL);
		if (FAILED(hr)) return hr;

		return true;
	}

	void FKTexture::SetTexture(UINT numSlot)
	{
		if (p_Tinfo.ps)
			p_OM->GetDrawContext()->PSSetShaderResources(numSlot, 1, &p_TextureRes);
		if (p_Tinfo.cs)
			p_OM->GetDrawContext()->CSSetShaderResources(numSlot, 1, &p_TextureRes);
	}

	void FKTexture::SetSampler(UINT numSlot)
	{
		p_OM->GetDrawContext()->PSSetSamplers(numSlot, 1, &p_Sampler);
	}

	bool FKTexture::InitForRendering()
	{
		
		D3D11_TEXTURE2D_DESC textureDesc;
		HRESULT result;
		D3D11_RENDER_TARGET_VIEW_DESC   renderTargetViewDesc;
		D3D11_SHADER_RESOURCE_VIEW_DESC shaderResourceViewDesc;
		ID3D11Texture2D*                renderTargetTexture;

		// Initialize the render target texture description.
		ZeroMemory(&textureDesc, sizeof(textureDesc));

		// Setup the render target texture description.
		textureDesc.Width = p_Tinfo.textureWidth;
		textureDesc.Height = p_Tinfo.textureHeight;
		textureDesc.MipLevels = 1;
		textureDesc.ArraySize = 1;
		textureDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		textureDesc.SampleDesc.Count = 1;
		textureDesc.Usage = D3D11_USAGE_DEFAULT;
		textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
		textureDesc.CPUAccessFlags = 0;
		textureDesc.MiscFlags = 0;

		// Create the render target texture.
		result = p_OM->GetDevice()->CreateTexture2D(&textureDesc, NULL, &renderTargetTexture);
		if (FAILED(result))
		{
			return false;
		}

		// Setup the description of the render target view.
		renderTargetViewDesc.Format = textureDesc.Format;
		renderTargetViewDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
		renderTargetViewDesc.Texture2D.MipSlice = 0;

		// Create the render target view.
		result = p_OM->GetDevice()->CreateRenderTargetView(renderTargetTexture, &renderTargetViewDesc, &p_TextureRenderTarget);
		if (FAILED(result))
		{
			return false;
		}

		// Setup the description of the shader resource view.
		shaderResourceViewDesc.Format = textureDesc.Format;
		shaderResourceViewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		shaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
		shaderResourceViewDesc.Texture2D.MipLevels = 1;

		// Create the shader resource view.
		result = p_OM->GetDevice()->CreateShaderResourceView(renderTargetTexture, &shaderResourceViewDesc, &p_TextureRes);
		if (FAILED(result))
		{
			return false;
		}

		SAFE_RELEASE(renderTargetTexture);

		if (p_Tinfo.zBuffer)
		{

			ID3D11Texture2D* backBuffer = NULL;
			ID3D11Texture2D* depthStencilBuffer = NULL;
			p_OM->GetSwapChain()->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBuffer);

			D3D11_TEXTURE2D_DESC descDepth;            // ��������� � �����������
			ZeroMemory(&descDepth, sizeof(descDepth));
			backBuffer->GetDesc(&descDepth);
			descDepth.MipLevels = 1;                   // ������� �����������
			descDepth.ArraySize = 1;
			descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT; // ������ (������ ������)
			descDepth.SampleDesc.Count = 1;
			descDepth.SampleDesc.Quality = 0;
			descDepth.Usage = D3D11_USAGE_DEFAULT;
			descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;         // ��� - ����� ������
			descDepth.CPUAccessFlags = 0;
			descDepth.MiscFlags = 0;
			SAFE_RELEASE(backBuffer);

			// ��� ������ ����������� ���������-������� ������� ������ ��������
			p_OM->GetDevice()->CreateTexture2D(&descDepth, NULL, &depthStencilBuffer);

			D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;            // ��������� � �����������
			ZeroMemory(&descDSV, sizeof(descDSV));
			descDSV.Format = descDepth.Format;         // ������ ��� � ��������
			descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
			descDSV.Texture2D.MipSlice = 0;
			// ��� ������ ����������� ���������-������� � �������� ������� ������ ������ ������
			p_OM->GetDevice()->CreateDepthStencilView(depthStencilBuffer, &descDSV, &p_DepthStencilView);

			SAFE_RELEASE(depthStencilBuffer);
		}

		return true;

	}

	void FKTexture::SetForRendering()
	{
		if(p_Tinfo.zBuffer)
		p_OM->GetDrawContext()->OMSetRenderTargets(1, &p_TextureRenderTarget, p_DepthStencilView);
		else
			p_OM->GetDrawContext()->OMSetRenderTargets(1, &p_TextureRenderTarget, NULL);

		p_OM->GetDrawContext()->ClearRenderTargetView(p_TextureRenderTarget, p_ClearColor);

		if (p_Tinfo.zBuffer)
		p_OM->GetDrawContext()->ClearDepthStencilView(p_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	}

	void FKTexture::Release()
	{
		SAFE_RELEASE(p_TextureRes);
		SAFE_RELEASE(p_Sampler);
		SAFE_RELEASE(p_CSTextureRes);
		SAFE_RELEASE(p_TextureRenderTarget);
		SAFE_RELEASE(p_DepthStencilView);
	}

	void FKTexture::CreateForCSOutput()
	{
		D3D11_TEXTURE2D_DESC blurredTexDesc;
		blurredTexDesc.Width = p_Tinfo.textureWidth;
		blurredTexDesc.Height = p_Tinfo.textureHeight;
		blurredTexDesc.MipLevels = 1;
		blurredTexDesc.ArraySize = 1;
		blurredTexDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		blurredTexDesc.SampleDesc.Count = 1;
		blurredTexDesc.SampleDesc.Quality = 0;
		blurredTexDesc.Usage = D3D11_USAGE_DEFAULT;
		blurredTexDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE |
			D3D11_BIND_UNORDERED_ACCESS;
		blurredTexDesc.CPUAccessFlags = 0;
		blurredTexDesc.MiscFlags = 0;

		ID3D11Texture2D* blurredTex = 0;
		p_OM->GetDevice()->CreateTexture2D(&blurredTexDesc, 0, &blurredTex);

		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		srvDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.MipLevels = 1;

		p_OM->GetDevice()->CreateShaderResourceView(blurredTex, &srvDesc, &p_TextureRes);

		D3D11_UNORDERED_ACCESS_VIEW_DESC uavDesc;
		uavDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
		uavDesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2D;
		uavDesc.Texture2D.MipSlice = 0;

		p_OM->GetDevice()->CreateUnorderedAccessView(blurredTex, &uavDesc, &p_CSTextureRes);


		SAFE_RELEASE(blurredTex);
	}

	void FKTexture::SetCSOutput(UINT numSlot)
	{
		p_OM->GetDrawContext()->CSSetUnorderedAccessViews(numSlot, 1, &p_CSTextureRes, NULL);
	}

}
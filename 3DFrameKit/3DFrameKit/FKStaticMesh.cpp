#include "FKStaticMesh.h"

namespace FrameKit
{

	FKStaticMesh::FKStaticMesh(FKObjectManager* om)
	{
		p_OM = om;
	}


	FKStaticMesh::~FKStaticMesh()
	{
	}

	HRESULT FKStaticMesh::SetData(void * l_date, UINT l_vCount, UINT l_byteWidth)
	{
		HRESULT hr = S_OK;

		p_VertexCount = l_vCount;

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = l_byteWidth;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;
		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = l_date;

		p_Stride = l_byteWidth / l_vCount;

		hr = p_OM->GetDevice()->CreateBuffer(&bd, &InitData, &p_VertexBuffer);
		if (FAILED(hr)) return hr;
		return hr;
	}

	void FKStaticMesh::Draw()
	{
		UINT offset = 0;
		p_OM->GetDrawContext()->IASetVertexBuffers(0, 1, &p_VertexBuffer, &p_Stride, &offset);

		p_OM->GetDrawContext()->Draw(p_VertexCount, 0);
	}

	void FKStaticMesh::Release()
	{
		SAFE_RELEASE(p_VertexBuffer);
	}

}
﻿#include "FKCamera.h"

namespace FrameKit
{

	FKCamera::FKCamera()
	{
	}



		XMFLOAT3 FKCamera::GetPosition()const
		{
			return mPosition;
		}

		void FKCamera::SetPosition(float x, float y, float z)
		{
			mPosition.x = x;
			mPosition.y = y;
			mPosition.z = z;

		}

		void FKCamera::SetViewMatrix(XMMATRIX* view)
		{
			mView = XMMatrixTranspose(*view);
		}

		void FKCamera::GetViewMatrix(XMMATRIX& m)
		{
			m = XMMatrixTranspose(mView);
		}

		void FKCamera::GetForvardVector(XMFLOAT3 & vec)
		{
			vec.x = mLook.x;
			vec.y = mLook.y;
			vec.z = mLook.z;

			float l = (vec.x*vec.x) + (vec.y*vec.y) + (vec.z*vec.z);
			l = sqrt(l);

			vec.x /= l;
			vec.y /= l;
			vec.z /= l;
		}

		void FKCamera::Walk(float d) {
			// mPosition += d*mLook 
			XMVECTOR s = XMVectorReplicate(d);
			XMVECTOR l = XMLoadFloat3(&mLook);
			XMVECTOR p = XMLoadFloat3(&mPosition);
			XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, l, p));
		}

		void FKCamera::Strafe(float d)
		{
			// mPosition += d*mRight 
			XMVECTOR s = XMVectorReplicate(d);
			XMVECTOR r = XMLoadFloat3(&mRight);
			XMVECTOR p = XMLoadFloat3(&mPosition);
			XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, r, p));
		}

		void FKCamera::Pitch(float angle)
		{ // Rotate up and look vector about the right vector.
			XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&mRight), angle);
			XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
			XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));
		}

		void FKCamera::LookAt(XMFLOAT3 pos, XMFLOAT3 target, XMFLOAT3 worldUp)
		{
			XMVECTOR t = XMVectorSet(target.x, target.y, target.z, 1);

			XMVECTOR p = XMVectorSet(pos.x, pos.y, pos.z, 1);

			XMVECTOR w = XMVectorSet(worldUp.x, worldUp.y, worldUp.z, 1);




			XMVECTOR L = XMVector3Normalize(XMVectorSubtract(t, p));
			XMVECTOR R = XMVector3Normalize(XMVector3Cross(w, L));
			XMVECTOR U = XMVector3Cross(L, R);

			XMStoreFloat3(&mPosition, p);
			XMStoreFloat3(&mLook, L);
			XMStoreFloat3(&mRight, R);
			XMStoreFloat3(&mUp, U);
		}

		void FKCamera::RotateY(float angle)
		{
			// Rotate the basis vectors about the world y-axis. 
			XMMATRIX R = XMMatrixRotationY(angle);
			XMStoreFloat3(&mRight, XMVector3TransformNormal(XMLoadFloat3(&mRight), R));
			XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
			XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));
		}

		void FKCamera::UpdateViewMatrix()
		{
			XMVECTOR R = XMLoadFloat3(&mRight);
			XMVECTOR U = XMLoadFloat3(&mUp);
			XMVECTOR L = XMLoadFloat3(&mLook);
			XMVECTOR P = XMLoadFloat3(&mPosition);
			// // Orthonormalize the right, up and look vectors. 
			// // Make look vector unit length. 
			L = XMVector3Normalize(L);
			// Compute a new corrected "up" vector and normalize it.
			U = XMVector3Normalize(XMVector3Cross(L, R));
			// Compute a new corrected "right" vector. U and L are // already ortho-normal, so no need to normalize cross product. // ||up × look|| = ||up|| ||look|| sin90° = 1 
			R = XMVector3Cross(U, L);
			// // Fill in the view matrix entries. // 
			float x = -XMVectorGetX(XMVector3Dot(P, R));
			float y = -XMVectorGetX(XMVector3Dot(P, U));
			float z = -XMVectorGetX(XMVector3Dot(P, L));
			XMStoreFloat3(&mRight, R);
			XMStoreFloat3(&mUp, U);
			XMStoreFloat3(&mLook, L);
			mView(0, 0) = mRight.x;
			mView(1, 0) = mRight.y;
			mView(2, 0) = mRight.z;
			mView(3, 0) = x;
			mView(0, 1) = mUp.x;
			mView(1, 1) = mUp.y;
			mView(2, 1) = mUp.z;
			mView(3, 1) = y;
			mView(0, 2) = mLook.x;
			mView(1, 2) = mLook.y;
			mView(2, 2) = mLook.z;
			mView(3, 2) = z;
			mView(0, 3) = 0.0f;
			mView(1, 3) = 0.0f;
			mView(2, 3) = 0.0f;
			mView(3, 3) = 1.0f;
		}

		

	}


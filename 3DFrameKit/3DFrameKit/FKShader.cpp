﻿#include "FKShader.h"

namespace FrameKit
{
	HRESULT FKShader::CompileShaderFromFile(WCHAR * szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob ** ppBlobOut)
	{
		HRESULT hr = S_OK;
		DWORD dwShaderFlags = D3DCOMPILE_ENABLE_STRICTNESS;
		ID3DBlob* pErrorBlob;
		hr = D3DX11CompileFromFileW(szFileName, NULL, NULL, szEntryPoint, szShaderModel,
			dwShaderFlags, 0, NULL, ppBlobOut, &pErrorBlob, NULL);
		if (FAILED(hr))
		{
			if (pErrorBlob != NULL)
				OutputDebugStringA((char*)pErrorBlob->GetBufferPointer());
			if (pErrorBlob) pErrorBlob->Release();
			return hr;
		}
		if (pErrorBlob) pErrorBlob->Release();

		return S_OK;
	}

	FrameKit::FKShader::FKShader(Sinfo * info, FKObjectManager * om)
	{
		HRESULT hr = S_OK;
		p_OM = om;

		if (info->vsInput)
		{
			hr = CompileShaderFromFile(info->fileName, info->vsInput, "vs_4_0", &p_SBlob);
			if (FAILED(hr))
			{
				ERROR((LPCSTR)hr);
			}

			hr = p_OM->GetDevice()->CreateVertexShader(p_SBlob->GetBufferPointer(), p_SBlob->GetBufferSize(), NULL, &p_VertexShader);
			if (FAILED(hr))
			{
				p_SBlob->Release();
				ERROR((LPCSTR)hr);
			}
		}

		if (info->psInput)
		{
			ID3DBlob* psBlob = NULL;
			hr = CompileShaderFromFile(info->fileName, info->psInput, "ps_4_0", &psBlob);
			if (FAILED(hr))
			{
				ERROR((LPCSTR)hr);
			}

			hr = p_OM->GetDevice()->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), NULL, &p_PixelShader);
			psBlob->Release();
			if (FAILED(hr)) ERROR((LPCSTR)hr);

		}


		if (info->csInput)
		{
			ID3DBlob* cBlob = NULL;
			hr = CompileShaderFromFile(info->fileName, info->csInput, "cs_5_0", &cBlob);
			if (FAILED(hr))
			{
				ERROR((LPCSTR)hr);
			}

			hr = p_OM->GetDevice()->CreateComputeShader(cBlob->GetBufferPointer(), cBlob->GetBufferSize(), NULL, &p_ComputeShader);
			cBlob->Release();
			if (FAILED(hr))  ERROR((LPCSTR)hr);

		}

	}

	FKShader::~FKShader()
	{
	}

	void FKShader::SetShader()
	{
		if (p_VertexShader)
		{
			p_OM->GetDrawContext()->VSSetShader(p_VertexShader, NULL, 0);
			p_OM->GetDrawContext()->IASetInputLayout(p_VertexLayoutList);
		}
		if (p_PixelShader)
			p_OM->GetDrawContext()->PSSetShader(p_PixelShader, NULL, 0);
		if (p_ComputeShader)
			p_OM->GetDrawContext()->CSSetShader(p_ComputeShader, NULL, 0);
	}

	void FKShader::SetVertexParam()
	{
		p_OM->GetDevice()->CreateInputLayout(p_Layout, p_VertexParamCount, p_SBlob->GetBufferPointer(),
			p_SBlob->GetBufferSize(), &p_VertexLayoutList);
	}

	void FKShader::Release()
	{
		SAFE_RELEASE(p_VertexLayoutList);
		SAFE_RELEASE(p_VertexShader);
		SAFE_RELEASE(p_ComputeShader);
		SAFE_RELEASE(p_PixelShader);
		SAFE_RELEASE(p_SBlob);
		this->~FKShader();
	}

}
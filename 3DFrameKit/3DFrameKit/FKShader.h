#pragma once
#include"General.h"
#include"FKObjectManager.h"
#include"FKVertexLayout.h"
#include<d3dcompiler.h>

namespace FrameKit
{

	class FKObjectManager;

	class FKShader : public FKVertexLayout, public FKObject
	{
		HRESULT CompileShaderFromFile(WCHAR* szFileName, LPCSTR szEntryPoint, LPCSTR szShaderModel, ID3DBlob** ppBlobOut);
		ID3D11VertexShader*     p_VertexShader  = NULL;
		ID3D11PixelShader*      p_PixelShader   = NULL;
		ID3D11ComputeShader*    p_ComputeShader = NULL;
		ID3DBlob*               p_SBlob         = NULL;
		FKObjectManager*        p_OM            = NULL;

	public:
		FKShader(Sinfo* info,FKObjectManager* om);
		~FKShader();
		void    SetShader();
		void    SetVertexParam();
		void    Release();

	};

}

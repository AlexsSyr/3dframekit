#pragma once
#include"FKObject.h"
#include"General.h"
#include"FKObjectManager.h"
namespace FrameKit
{

	class FKObjectManager;

	class FKConstantBuffer : public FKObject
	{
	public:
		~FKConstantBuffer();

		FKConstantBuffer(CBinfo* info, FKObjectManager* om);
		void SetData(void* data);
		void SetBuffer(UINT num);
		void Release();

	private:
		ID3D11Buffer*       p_ConstBuffer = NULL;
		FKObjectManager*    p_OM          = NULL;
		bool                p_PS;
		bool                p_VS;
	};

}

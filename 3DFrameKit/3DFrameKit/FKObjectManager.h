#pragma once
#include"FKObject.h"
#include<vector>
#include"General.h"
#include"FKRenderTarget.h"
#include"FKShader.h"
#include"FKConstantBuffer.h"
#include"FKStaticMesh.h"
#include"FKTexture.h"
#include"FKStructuredBuffer.h"
using namespace std;

namespace FrameKit
{

	class FKRenderTarget;
	class FKShader;
	class FKConstantBuffer;
	class FKStaticMesh;
	class FKTexture;
	class FKStructuredBuffer;

	class FKObjectManager 
	{
		D3D_FEATURE_LEVEL        p_FeatureLevel = D3D_FEATURE_LEVEL_11_0;                      //������������ ������
		ID3D11Device*            p_Device = NULL;		                                         //���������� ��������
		ID3D11DeviceContext*     p_DrawContext = NULL;	                                     //���������� ���������
		IDXGISwapChain*          p_SwapChain = NULL;		                                     //���� ��������
		D3D_DRIVER_TYPE          p_DriverType = D3D_DRIVER_TYPE_NULL;                          //��� ����������           
		D3D11_PRIMITIVE_TOPOLOGY p_PrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		ID3D11Debug*             p_Debug = NULL;

		//������ ��������� ��������
		vector<FKObject*> p_ObjectList;          

	public:
		FKObjectManager(OMinfo* info);
		~FKObjectManager();

		ID3D11Device*        GetDevice();
		IDXGISwapChain*      GetSwapChain();
		ID3D11DeviceContext* GetDrawContext();

		void SetViewPort(float w, float h, int x, int y);
		void CreateRT(RTinfo* info, FKRenderTarget*& rt);
		void CreateS(Sinfo* info, FKShader*& s);
		void CreateCB(CBinfo* info, FKConstantBuffer*& cb);
		void CreateSM(FKStaticMesh*& sm);
		void CreateT(Tinfo* info, FKTexture*& t);
		void CreateSB(SBinfo* info, FKStructuredBuffer*& sb);

	};
}


#pragma once
#include"General.h"
#include"FKObjectManager.h"

namespace FrameKit
{
	class FKObjectManager;

	class FKStaticMesh :public  FKObject
	{
	public:
		FKStaticMesh(FKObjectManager* om);
		~FKStaticMesh();

		HRESULT SetData(void * date, UINT vCount, UINT byteWidth);
		void    Draw();
		void    Release();

	private:
		ID3D11Buffer*           p_VertexBuffer = NULL;
		UINT                    p_VertexCount  = 0;
		UINT                    p_Stride       = 0;
		FKObjectManager*        p_OM           = NULL;

	};

}
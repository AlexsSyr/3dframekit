#include "FKTimer.h"


namespace FrameKit
{

	FKTimer::FKTimer()
		: mSecondsPerCount(0.0), mDeltaTime(-1.0), mBaseTime(0),
		mPausedTime(0), mPrevTime(0), mCurrTime(0), mStopped(false)
	{
		__int64 countsPerSec;
		QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSec);
		mSecondsPerCount = 1.0 / (double)countsPerSec;
	}


	FKTimer::~FKTimer()
	{
	}

	float FKTimer::TotalTime()const
	{
		// If we are stopped, do not count the time that has passed since we stopped.
		// Moreover, if we previously already had a pause, the distance 
		// mStopTime - mBaseTime includes paused time, which we do not want to count.
		// To correct this, we can subtract the paused time from mStopTime:  
		//
		//                     |<--paused time-->|
		// ----*---------------*-----------------*------------*------------*------> time
		//  mBaseTime       mStopTime        startTime     mStopTime    mCurrTime

		if (mStopped)
		{
			return (float)(((mStopTime - mPausedTime) - mBaseTime)*mSecondsPerCount);
		}

		// The distance mCurrTime - mBaseTime includes paused time,
		// which we do not want to count.  To correct this, we can subtract 
		// the paused time from mCurrTime:  
		//
		//  (mCurrTime - mPausedTime) - mBaseTime 
		//
		//                     |<--paused time-->|
		// ----*---------------*-----------------*------------*------> time
		//  mBaseTime       mStopTime        startTime     mCurrTime

		else
		{
			return (float)(((mCurrTime - mPausedTime) - mBaseTime)*mSecondsPerCount);
		}
	}

	float FKTimer::DeltaTime()const
	{
		return (float)mDeltaTime;
	}

	void FKTimer::Reset()
	{
		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

		mBaseTime = currTime;
		mPrevTime = currTime;
		mStopTime = 0;
		mStopped = false;
	}

	void FKTimer::Start()
	{
		__int64 startTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&startTime);


		// Accumulate the time elapsed between stop and start pairs.
		//
		//                     |<-------d------->|
		// ----*---------------*-----------------*------------> time
		//  mBaseTime       mStopTime        startTime     

		if (mStopped)
		{
			mPausedTime += (startTime - mStopTime);

			mPrevTime = startTime;
			mStopTime = 0;
			mStopped = false;
		}
	}

	void FKTimer::Stop()
	{
		if (!mStopped)
		{
			__int64 currTime;
			QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

			mStopTime = currTime;
			mStopped = true;
		}
	}

	void FKTimer::Tick()
	{
		if (mStopped)
		{
			mDeltaTime = 0.0;
			return;
		}

		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
		mCurrTime = currTime;

		// Time difference between this frame and the previous.
		mDeltaTime = (mCurrTime - mPrevTime)*mSecondsPerCount;

		// Prepare for next frame.
		mPrevTime = mCurrTime;

		// Force nonnegative.  The DXSDK's CDXUTTimer mentions that if the 
		// processor goes into a power save mode or we get shuffled to another
		// processor, then mDeltaTime can be negative.
		if (mDeltaTime < 0.0)
		{
			mDeltaTime = 0.0;
		}
	}

	void FKTimer::CalculateFPS(HWND hwnd)
	{
		static int frameCnt = 0;
		static float timeElapsed = 0.0f;
		frameCnt++;
		// Compute averages over one second period. 
		if ((TotalTime() - timeElapsed) >= 1.0f)
		{
			float fps = (float)frameCnt;
			// fps = frameCnt / 1 
			float mspf = 1000.0f / fps;
			std::wostringstream outs;
			outs.precision(6);
			outs << WINDOW_TITLE << L" " << L"FPS: " << fps << L" " << L"Frame Time: " << mspf << L" (ms)";
			SetWindowText(hwnd, (LPCWSTR)outs.str().c_str()); // Reset for next average.
			frameCnt = 0;
			timeElapsed += 1.0f;
		}


	}



}

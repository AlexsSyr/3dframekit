#pragma once
#include"General.h"
#include"FKObjectManager.h"

namespace FrameKit
{

	class FKObjectManager;

	class FKStructuredBuffer : public FKObject
	{
	public:
		FKStructuredBuffer(SBinfo* info, FKObjectManager* om);
		void SetData(void* data);
		void SetBuffer(UINT l_num);
		void Release();

	private:
		FKObjectManager*           p_OM      = NULL;
		ID3D11Buffer*              p_Buffer  = NULL;
		ID3D11UnorderedAccessView* p_CSRWRes = NULL;
		ID3D11ShaderResourceView*  p_CSRes   = NULL;
		bool                       p_ForWrite;
	};
}

